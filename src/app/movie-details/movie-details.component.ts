import { Movie } from './../movie';
import { Component, OnInit, Input } from '@angular/core';
import { MovieService } from '../movie.service';
import { MovieListComponent } from '../movie-list/movie-list.component';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  @Input() movie: Movie;
  movies: Observable<Movie[]>;
  totalSize: number;
  pageCount: number[] = [];
  selectedPage: number;

  constructor(private movieService: MovieService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
    this.movieService.getMoviesList().subscribe((res) => {
      this.totalSize = res.length;
      const count = this.totalSize % 5;
      for (let i = 0; i < count; i++) {
        this.pageCount[i] = i + 1;
      }
      console.log('pageCount', this.pageCount.length);
      this.selectedPage = 1;
    });
    this.movies = this.movieService.getMovieListByPage(0, 5);
  }

  loadData(page: number): void {
    this.selectedPage = page - 1;
    this.movies = this.movieService.getMovieListByPage(page - 1, 5);
  }


}
