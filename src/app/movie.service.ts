import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private baseUrl = '/api/auth/movies';
  private getByPage = 'http://localhost:8080/api/auth/movies/page';

  constructor(private http: HttpClient) { }

  getMovie(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createMovie(movie: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, movie);
  }

  updateMovie(value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/update`, value);
  }

  deleteMovie(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getMoviesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  getMovieListByPage(page, size): Observable<any> {
    return this.http.get(this.getByPage + '/' + page + '/' + size);
  }
}
