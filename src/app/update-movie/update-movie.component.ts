import { Component, OnInit } from '@angular/core';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-update-movie',
  templateUrl: './update-movie.component.html',
  styleUrls: ['./update-movie.component.css']
})
export class UpdateMovieComponent implements OnInit {

  movie: Movie;
  movieId: string;
  submitted = false;

  constructor(
    private _movieService: MovieService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.movieId = params['id'];
	  console.log(this.movieId);
      this.loadDress();
    });
  }

  loadDress(): void {
    this._movieService.getMovie(parseInt(this.movieId)).subscribe((res: any) => {
      this.movie = res;
    });
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  save() {
    this._movieService.updateMovie(this.movie)
      .subscribe(data => console.log(data), error => console.log(error));
  }
}
