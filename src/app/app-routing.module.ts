import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { PmComponent } from './pm/pm.component';
import { AdminComponent } from './admin/admin.component';
import {MovieListComponent} from './movie-list/movie-list.component';
import {CreateMovieComponent} from './create-movie/create-movie.component';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {UpdateMovieComponent} from './update-movie/update-movie.component';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'user',
        component: UserComponent
    },
    {
        path: 'pm',
        component: PmComponent
    },
    {
        path: 'admin',
        component: AdminComponent
    },
    {
        path: 'auth/login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: RegisterComponent
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'movies',
        component: MovieListComponent
    },
  { path: 'add', component: CreateMovieComponent },

  { path: 'update', component: UpdateMovieComponent },

  { path: 'update/movie/:id', component: UpdateMovieComponent },
  { path: 'details', component: MovieDetailsComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
