import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {Observable} from 'rxjs';
import {Movie} from '../movie';
import {MovieService} from '../movie.service';
import {Router} from '@angular/router';
import {ModalService} from '../_services';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  movies: Movie[];
  bodyText: string;
  selectedId: any;
  closeResult: string;
  constructor(private movieService2: MovieService,
              private router: Router,
			   private modalService: NgbModal) {
  }

  ngOnInit() {
    this.bodyText = 'This text can be updated in modal 1';
    this.reloadData();
  }

  reloadData() {
    this.movieService2.getMoviesList().subscribe(res=>{
		this.movies = res;
		console.log(this.movies);
	});
	  }

  deleteMovie(id: number) {
    this.movieService2.deleteMovie(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
  
  open(content, selectedId) {
	  this.selectedId = selectedId;
	  console.log('seletedId: ', selectedId);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

 
}
