export class Movie {
  id: number;
  title: string;
  country: string;
  length: string;
  description: string;
  price: string;
}
