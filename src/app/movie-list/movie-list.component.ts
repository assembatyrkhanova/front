import { MovieDetailsComponent } from './../movie-details/movie-details.component';
import { Observable } from 'rxjs';
import { MovieService } from '../movie.service';
import { Movie } from '../movie';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movies: Movie[];

  constructor(private movieService: MovieService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
     this.movieService.getMoviesList().subscribe(res=>{
		this.movies = res;
	});
  }

  deleteMovie(id: number) {
    this.movieService.deleteMovie(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
